package cn.itmtr.document.download.common.util;


import cn.itmtr.document.download.common.exception.ItmtrException;
import cn.itmtr.document.download.common.exception.ItmtrExceptionEnum;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 返回值
 *
 * @author mtr
 * @since 2021-08-08
 */
@Data
public class Result {

    /**
     * 状态码
     */
    public Integer code;

    /**
     * 提示消息
     */
    public String msg;

    /**
     * 数据
     */
    public Map<String, Object> data;

    public Result() {
        this(ItmtrExceptionEnum.SUCCESS);
    }

    public Result(ItmtrExceptionEnum exceptionEnum) {
        this(exceptionEnum.getCode(), exceptionEnum.getMessage());
    }

    public Result(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = new HashMap<>(5, 1);
    }

    /**
     * 向data中放入数据
     *
     * @param key 键
     * @param obj 值
     * @return cn.itmtr.user.common.result.Result
     * @author mtr
     * @date 2021/8/8
     */
    public Result put(String key, Object obj) {
        data.put(key, obj);
        return this;
    }

    /**
     * 向data中放入数据 以data为键
     *
     * @param data 数据
     * @return cn.itmtr.user.common.result.Result
     * @author mtr
     * @date 2021/8/8
     */
    public Result putData(Object data) {
        return this.put("data", data);
    }

    public static Result ok() {
        return new Result();
    }

    public static Result ok(Object data) {
        return new Result().putData(data);
    }

    public static Result ok(String key, Object obj) {
        return new Result().put(key, obj);
    }

    public static Result error(String errorMsg) {
        return new Result(ItmtrExceptionEnum.ERROR.getCode(), errorMsg);
    }

    public static Result error(Integer code, String msg) {
        return new Result(code, msg);
    }

    public static Result error(ItmtrExceptionEnum exceptionEnum) {
        return new Result(exceptionEnum);
    }

    public static Result error(ItmtrException e) {
        return new Result(e.getCode(), e.getMessage());
    }

}
