package cn.itmtr.document.download.common.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.system.ApplicationHome;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;

/**
 * 自定义获取配置方法
 *
 * @author mtr
 * @date 2021/3/25
 */
@Slf4j
@Component
public class TdEnvBeanUtil implements EnvironmentAware {

    private static Environment env;

    @Override
    public void setEnvironment(Environment environment) {
        env = environment;
    }

    public static String getString(String key) {
        return env.getProperty(key);
    }

    /**
     * 获取项目jar包所在路径
     *
     * @return java.lang.String
     * @author mtr
     * @date 2021/10/4
     */
    public static String getProjectJarPath() {
        ApplicationHome ah = new ApplicationHome(TdEnvBeanUtil.class);
        File file = ah.getSource();
        if (file != null) {
            return file.getParentFile().toString();
        } else {
            return ah.getDir().toString();
        }
    }

}
