package cn.itmtr.document.download.common.util;

/**
 * 链接类型
 *
 * @author mtr
 * @since 2021-10-04
 */
public enum UrlPropertyTypeEnum {

    HTML("html"),
    JS("js"),
    CSS("css"),
    IMAGE("image"),
    VIDEO("video"),
    AUDIO("audio"),
    ZIP("zip"),
    GIT("git")
    ;

    private String type;

    public String getType() {
        return type;
    }

    UrlPropertyTypeEnum(String type) {
        this.type = type;
    }

}
