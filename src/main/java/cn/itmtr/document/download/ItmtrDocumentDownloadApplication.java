package cn.itmtr.document.download;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author mtr
 * @date 2021/9/30
 */
@SpringBootApplication
public class ItmtrDocumentDownloadApplication {

    public static void main(String[] args) {
        SpringApplication.run(ItmtrDocumentDownloadApplication.class, args);
    }

}
