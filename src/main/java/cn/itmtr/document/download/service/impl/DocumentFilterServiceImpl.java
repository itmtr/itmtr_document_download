package cn.itmtr.document.download.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import cn.itmtr.document.download.mapper.DocumentFilterMapper;
import cn.itmtr.document.download.model.dto.DocumentFilterDTO;
import cn.itmtr.document.download.model.entity.DocumentFilter;
import cn.itmtr.document.download.service.IDocumentFilterService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author mtr
 * @since 2021-10-06
 */
@Slf4j
@Service
public class DocumentFilterServiceImpl extends ServiceImpl<DocumentFilterMapper, DocumentFilter> implements IDocumentFilterService {

    @Override
    public List<DocumentFilter> listByDocumentId(Integer documentId) {
        return this.list(
                new QueryWrapper<DocumentFilter>()
                        .eq("document_id", documentId)
                        .orderByDesc("gmt_create")
        );
    }

    @Override
    public void create(DocumentFilterDTO filterDTO) {
        DocumentFilter filter = new DocumentFilter();
        filter.setDocumentId(filterDTO.getDocumentId());
        filter.setDocumentName(filterDTO.getDocumentName());
        filter.setFilterType(filterDTO.getFilterType());
        filter.setSourceContent(filterDTO.getSourceContent());
        filter.setTargetContent(filterDTO.getTargetContent());
        filter.setRegexFlag(filterDTO.getRegexFlag() ? 1 : 0);
        filter.setGmtCreate(DateUtil.now());
        filter.setGmtUpdate(DateUtil.now());
        this.save(filter);
    }

    @Override
    public String filterDocumentHtml(List<DocumentFilter> documentFilters, String html) {
        for (DocumentFilter filter : documentFilters) {
            boolean regexFlag = filter.getRegexFlag() == 1;
            switch (filter.getFilterType()) {
                case "REMOVE":
                    html = filterDocumentHtmlRemove(html, regexFlag, filter.getSourceContent());
                    break;
                case "REPLACE":
                    html = filterDocumentHtmlReplace(html, regexFlag, filter.getSourceContent(), filter.getTargetContent());
                    break;
                default:
            }
        }
        return html;
    }

    /**
     * 过滤操作 删除
     *
     * @param html          富文本内容
     * @param regexFlag     是否使用正则
     * @param sourceContent 要去除的内容
     * @return java.lang.String
     * @author mtr
     * @date 2021/10/9
     */
    private String filterDocumentHtmlRemove(String html, boolean regexFlag, String sourceContent) {
        if (regexFlag) {
            return html.replaceAll(sourceContent, "");
        } else {
            return StrUtil.removeAll(html, sourceContent);
        }
    }

    /**
     * 过滤操作 替换
     *
     * @param html          富文本内容
     * @param regexFlag     是否使用正则
     * @param sourceContent 被替换的内容
     * @param targetContent 替换的内容
     * @return java.lang.String
     * @author mtr
     * @date 2021/10/9
     */
    private String filterDocumentHtmlReplace(String html, boolean regexFlag, String sourceContent, String targetContent) {
        if (regexFlag) {
            return html.replaceAll(sourceContent, targetContent);
        } else {
            return StrUtil.replace(html, sourceContent, targetContent);
        }
    }


}
