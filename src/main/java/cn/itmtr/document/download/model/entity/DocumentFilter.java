package cn.itmtr.document.download.model.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author mtr
 * @since 2021-10-06
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("document_filter")
public class DocumentFilter implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("document_id")
    private Integer documentId;

    @TableField("document_name")
    private String documentName;

    /**
     * 过滤类型 REMOVE REPLACE
     */
    @TableField("filter_type")
    private String filterType;

    @TableField("source_content")
    private String sourceContent;

    @TableField("target_content")
    private String targetContent;

    /**
     * 是否使用正则 0不使用 1使用正则
     */
    @TableField("regex_flag")
    private Integer regexFlag;

    @TableField("gmt_create")
    private String gmtCreate;

    @TableField("gmt_update")
    private String gmtUpdate;


}
