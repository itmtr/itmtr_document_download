package cn.itmtr.document.download.model.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * 过滤内容控制器
 *
 * @author mtr
 * @since 2021-10-06
 */
@Data
public class DocumentFilterDTO {

    @NotNull(message = "请先选择文档")
    private Integer documentId;

    @NotBlank(message = "请先选择文档")
    private String documentName;

    /**
     * 过滤类型 REMOVE REPLACE
     */
    @NotBlank(message = "请选择过滤类型")
    private String filterType;

    @NotBlank(message = "请输入内容")
    private String sourceContent;

    private String targetContent;

    @NotNull(message = "请设置是否使用正则表达式")
    private Boolean regexFlag;

}
