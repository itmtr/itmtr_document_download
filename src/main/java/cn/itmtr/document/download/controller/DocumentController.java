package cn.itmtr.document.download.controller;


import cn.itmtr.document.download.common.util.Result;
import cn.itmtr.document.download.common.validator.Assert;
import cn.itmtr.document.download.model.entity.Document;
import cn.itmtr.document.download.service.IDocumentResourcesService;
import cn.itmtr.document.download.service.IDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author mtr
 * @since 2021-10-04
 */
@Controller
public class DocumentController {

    @Autowired
    public IDocumentService documentService;

    @Autowired
    public IDocumentResourcesService documentResourcesService;

    @Autowired
    @Qualifier("customThreadExecutor")
    private Executor executor;

    /**
     * 主页
     *
     * @return java.lang.String
     * @author mtr
     * @date 2021/10/4
     */
    @GetMapping("")
    public String index() {
        return "index";
    }

    /**
     * 获取文档列表
     *
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/5
     */
    @ResponseBody
    @GetMapping("/documentList")
    public Result documentList() {
        List<Document> list = documentService.list();
        return Result.ok("documentList", list);
    }

    /**
     * 创建文档
     *
     * @param documentName 文档名称
     * @param url          链接地址
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/4
     */
    @ResponseBody
    @PostMapping("/createDocument")
    public Result createDocument(String documentName, String url) {
        Document document = documentService.createDocument(documentName, url);
        return Result.ok(document);
    }

    /**
     * 开始下载
     *
     * @param documentId 文搭id
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/4
     */
    @ResponseBody
    @PostMapping("/startDownload")
    public Result startDownload(Integer documentId) {
        Document document = documentService.getById(documentId);
        Assert.isNull(document, "未找到文档");
        Assert.isTrue(document.getStatus() == 1, "正在下载，请稍后在试");
        // 修改状态为进行中
        documentService.updateStatus(documentId, 1);
        // 开始下载
        executor.execute(() -> documentResourcesService.downloadResources(document));
        return Result.ok();
    }

    /**
     * 手动终止下载
     *
     * @param documentId 文档id
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/5
     */
    @ResponseBody
    @PostMapping("/stopDownload")
    public Result stopDownload(Integer documentId) {
        Document document = documentService.getById(documentId);
        Assert.isNull(document, "未找到文档");
        // 修改状态为手动终止
        documentService.updateStatus(document.getId(), 3);
        return Result.ok();
    }

    /**
     * 下载文档
     *
     * @param response   response
     * @param documentId 文档id
     * @param userAgent  ua
     * @author mtr
     * @date 2021/10/5
     */
    @ResponseBody
    @GetMapping("/downloadDocument")
    public void downloadDocument(HttpServletResponse response, Integer documentId,
                                 @RequestHeader(value = "user-agent") String userAgent) {
        Document document = documentService.getById(documentId);
        Assert.isNull(document, "未找到文档");
        documentService.downloadDocument(document, response, userAgent);
    }

    /**
     * 重新开始下载文档
     *
     * @param documentId 文档id
     * @return cn.itmtr.document.download.common.util.Result
     * @author mtr
     * @date 2021/10/18
     */
    @ResponseBody
    @PostMapping("/restartDownload")
    public Result restartDownload(Integer documentId) {
        Document document = documentService.getById(documentId);
        Assert.isNull(document, "未找到文档");
        Assert.isTrue(document.getStatus() == 1, "正在下载，请稍后在试");
        // 清楚相关数据
        documentResourcesService.removeByDocumentId(documentId);
        documentService.removeFile(document);
        // 修改状态为进行中
        documentService.updateStatus(documentId, 1);
        // 开始下载
        executor.execute(() -> documentResourcesService.downloadResources(document));
        return Result.ok();
    }

}
