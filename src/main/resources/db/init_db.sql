-- 初始化数据库

drop table document;
create table document -- 文档表
(
    id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    document_name   TEXT    DEFAULT '' NOT NULL, -- 文档名称
    url             TEXT    DEFAULT '' NOT NULL, -- 来源链接
    protocol        TEXT    DEFAULT '' NOT NULL, -- 协议 http 或 https
    domain          TEXT    DEFAULT '' NOT NULL, -- 域名
    root_path       TEXT    DEFAULT '' NOT NULL, -- 保存根目录
    status          INTEGER DEFAULT 0 NOT NULL,  -- 下载状态 0待开始 1正在执行 2执行结束
    gmt_create      TEXT    DEFAULT '' NOT NULL, -- 创建时间
    gmt_update      TEXT    DEFAULT '' NOT NULL  -- 更新时间
);

drop table document_filter;
create table document_filter -- 文档内容过滤表
(
    id             INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    document_id    INTEGER DEFAULT -1 NOT NULL, -- 文档id
    document_name  TEXT    DEFAULT '' NOT NULL, -- 文档名称
    filter_type    TEXT    DEFAULT '' NOT NULL, -- 过滤类型 REMOVE REPLACE
    source_content TEXT    DEFAULT '' NOT NULL, -- 原内容
    target_content TEXT    DEFAULT '' NOT NULL, -- 目标内容
    regex_flag     INTEGER DEFAULT 0 NOT NULL,  -- 是否使用正则 0不适用 1使用正则
    gmt_create     TEXT    DEFAULT '' NOT NULL, -- 创建时间
    gmt_update     TEXT    DEFAULT '' NOT NULL  -- 更新时间
);


drop table document_resources;
create table document_resources -- 文档资源表
(
    id              INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    document_id     INTEGER DEFAULT -1 NOT NULL, -- 文档id
    source_url_key  TEXT    DEFAULT '' NOT NULL, -- 来源链接key
    source_url      TEXT    DEFAULT '' NOT NULL, -- 来源链接
    target_url      TEXT    DEFAULT '' NOT NULL, -- 最终访问的链接
    protocol        TEXT    DEFAULT '' NOT NULL, -- 协议 http 或 https
    domain          TEXT    DEFAULT '' NOT NULL, -- 域名
    uri             TEXT    DEFAULT '' NOT NULL, -- 地址
    query           TEXT    DEFAULT '' NOT NULL, -- 参数
    html_content    TEXT    DEFAULT '' NOT NULL, -- 网页html内容
    html_template   TEXT    DEFAULT '' NOT NULL, -- 网页html模板内容
    type            TEXT    DEFAULT '' NOT NULL, -- 资源类型 html js css image video audio
    path            TEXT    DEFAULT '' NOT NULL, -- 资源保存路径
    page_path       TEXT    DEFAULT '' NOT NULL, -- 页面资源保存路径
    download_status INTEGER DEFAULT 0 NOT NULL,  -- 下载状态 0等下载 1下载成功 2下载失败
    gmt_create      TEXT    DEFAULT '' NOT NULL, -- 创建时间
    gmt_update      TEXT    DEFAULT '' NOT NULL  -- 更新时间
);
