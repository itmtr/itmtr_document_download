package cn.itmtr.document.download;

import cn.itmtr.document.download.model.entity.Document;
import cn.itmtr.document.download.service.IDocumentResourcesService;
import cn.itmtr.document.download.service.IDocumentService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * 测试类
 *
 * @author mtr
 * @since 2021-10-04
 */
@SpringBootTest
public class ItmtrDocumentDownloadApplicationTests {

    @Autowired
    public IDocumentService documentService;

    @Autowired
    public IDocumentResourcesService documentResourcesService;

    @Test
    public void testCreateDocument() {
        documentService.createDocument("Layui", "https://www.layui.com");
    }


    @Test
    public void testStartDownload() {
        Document document = documentService.getById(1);
        // 开始下载
        documentResourcesService.downloadResources(document);
    }


}
